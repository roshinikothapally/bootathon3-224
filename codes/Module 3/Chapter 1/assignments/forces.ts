var s1: HTMLSelectElement=<HTMLSelectElement>document.getElementById("s1");
var tb1: HTMLTableElement=<HTMLTableElement>document.getElementById("tb1");
var tb2: HTMLTableElement=<HTMLTableElement>document.getElementById("tb2");

//adding number of rows to select tag
for(let i=2; i<=6;i++)
{
    var option: HTMLOptionElement=<HTMLOptionElement>document.createElement("option");
    option.text = i.toString();
    option.value = i.toString();
    s1.add(option);
}

//onchange select tag create input table
function createinput()
{
    //delete table
    deleteinputs(tb1);
   //create input table
    createtable(tb1,"a")
    
}
//delete all rows
function deleteinputs(t: HTMLTableElement)
{
    while(t.rows.length >0){
        t.deleteRow(0);
    }

}
// create rows and columns
function createtable(t: HTMLTableElement,id:string){
    var row = +s1.value;
    var col = 2;
    for(let i=0; i<row;i++){
        var trow: HTMLTableRowElement=t.insertRow();
        for(let j=0; j<col;j++){ // j is for creating cols.
            var tcell= trow.insertCell();
            var text: HTMLInputElement = <HTMLInputElement>document.createElement("input");
            text.id =id+i+j;
            text.type="text";
            tcell.appendChild(text);
        }
        
    }
}
//button onclick resultant
function resultant(){
    var a:number[][] = []; //read force and angle
    readtable(a, +s1.value,2,"a");
    calculate(a);// calcuting resultant

}
//read the numbers from rows and columns
function readtable(a: number[][], row: number,col:number, id:string){
    for(let i=0; i<row;i++){
        a[i]=[];
        for(let j=0; j<col; j++){
            let t1: HTMLInputElement = <HTMLInputElement>document.getElementById(id + i +j);
            a[i][j]= +t1.value;
        }
        
    }
}
// calculating resultant of forces
function calculate(a: number[][]){
    var sx:number = 0;
    var sy:number = 0;

    for(let i=0; i<+s1.value; i++){
        sx+= a[i][0] + Math.cos(Math.PI/180 * a[i][1]); //summation of x
        sy+= a[i][0] + Math.cos(Math.PI/180 * a[i][1]); //summation of y
    }
    var result = Math.sqrt(Math.pow(sx,2)+Math.pow(sy,2));//resultant magnitude
    var angl =(180/Math.PI)* Math.atan2(sy, sx)//resultant angle
    console.log("Magnitude",result);
    console.log("Angle",angl* 180 / Math.PI);

}

