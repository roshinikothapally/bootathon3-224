class Curve{

    private canvas:HTMLCanvasElement;
    private context:CanvasRenderingContext2D;
    public x:number;
    public y: number;
    public amp =75;


    constructor(x:number,y:number,canvas:HTMLCanvasElement,context:CanvasRenderingContext2D){
        
        this.x = x;
        this.y = y;
        this.canvas = canvas;
        this.context = context;
    }

    draw()
    {
        console.log("hello");

        this.context.beginPath();
        this.context.moveTo(this.x,this.y-75);
        this.context.lineTo(this.x,this.y+75);
        this.context.moveTo(this.x-75,this.y);
        this.context.lineTo(this.x+360,this.y);

        this.context.lineWidth = 2;
        this.context.strokeStyle = 'black';
        this.context.stroke();
        var xo = this.x;
        var yo = this.y;
        this.context.beginPath();
        this.context.lineTo(xo,yo+this.amp* Math.sin(Math.PI /100));

        for(var i=0;i< 360;i++)
        {
            this.context.lineTo(xo+i,yo+this.amp * Math.sin(Math.PI *i/100));
        }

        this.context.lineWidth = 2.5;
        this.context.strokeStyle = 'blue';
        this.context.stroke();
        
        this.context.beginPath();
        this.context.lineTo(xo,yo+this.amp* Math.cos(Math.PI /100));

        for(var i=0;i< 360;i++)
        {
            this.context.lineTo(xo+i,yo+this.amp * Math.cos(Math.PI *i/100));
        }

        this.context.lineWidth = 2.5;
        this.context.strokeStyle = 'green';
        this.context.stroke();

        this.context.beginPath();
        this.context.moveTo(600,200);
        this.context.lineTo(800,200);
        this.context.moveTo(700,200);
        this.context.lineTo(700,400);
        this.context.stroke();

        this.context.beginPath();
        this.context.arc(700,400,50,0,2*Math.PI,false);
        
        this.context.strokeStyle = 'green';
        this.context.fillStyle = 'gray';
        this.context.fill();
        this.context.stroke();

    }
}


