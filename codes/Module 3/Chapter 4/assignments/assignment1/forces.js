var s1 = document.getElementById("s1");
var tb1 = document.getElementById("tb1");
var tb2 = document.getElementById("tb2");
//adding number of rows to select tag
for (let i = 2; i <= 6; i++) {
    var option = document.createElement("option");
    option.text = i.toString();
    option.value = i.toString();
    s1.add(option);
}
//onchange select tag create input table
function createinput() {
    //delete table
    deleteinputs(tb1);
    //create input table
    createtable(tb1, "a");
}
//delete all rows
function deleteinputs(t) {
    while (t.rows.length > 0) {
        t.deleteRow(0);
    }
}
// create rows and columns
function createtable(t, id) {
    var row = +s1.value;
    var col = 2;
    for (let i = 0; i < row; i++) {
        var trow = t.insertRow();
        for (let j = 0; j < col; j++) { // j is for creating cols.
            var tcell = trow.insertCell();
            var text = document.createElement("input");
            text.id = id + i + j;
            text.type = "text";
            tcell.appendChild(text);
        }
    }
}
//button onclick resultant
function resultant() {
    var a = []; //read force and angle
    readtable(a, +s1.value, 2, "a");
    calculate(a); // calcuting resultant
}
//read the numbers from rows and columns
function readtable(a, row, col, id) {
    for (let i = 0; i < row; i++) {
        a[i] = [];
        for (let j = 0; j < col; j++) {
            let t1 = document.getElementById(id + i + j);
            a[i][j] = +t1.value;
        }
    }
}
// calculating resultant of forces
function calculate(a) {
    var sx = 0;
    var sy = 0;
    for (let i = 0; i < +s1.value; i++) {
        sx += a[i][0] + Math.cos(Math.PI / 180 * a[i][1]); //summation of x
        sy += a[i][0] + Math.cos(Math.PI / 180 * a[i][1]); //summation of y
    }
    var result = Math.sqrt(Math.pow(sx, 2) + Math.pow(sy, 2)); //resultant magnitude
    var angl = (180 / Math.PI) * Math.atan2(sy, sx); //resultant angle
    console.log("Magnitude", result);
    console.log("Angle", angl * 180 / Math.PI);
}
//# sourceMappingURL=forces.js.map