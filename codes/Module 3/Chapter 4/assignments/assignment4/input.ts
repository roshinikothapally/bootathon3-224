function input() {

    var canvas:HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
    var context:CanvasRenderingContext2D = canvas.getContext("2d");
    context.clearRect(0, 0,canvas.width, canvas.height );
    var input:HTMLInputElement = <HTMLInputElement>document.getElementById("a");
    let dial:Dial = new Dial(input, canvas, context);
    dial.draw();

}