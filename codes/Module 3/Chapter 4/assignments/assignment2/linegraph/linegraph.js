var datapoints1 = [];
var datapoints2 = [];
datapoints1.push({ x: 1, y: 1 }); //coordinates of a    
datapoints1.push({ x: 2, y: 1 }); //coordinates of b
datapoints1.push({ x: 2, y: 2 }); //coordinates of c
datapoints1.push({ x: 1, y: 2 }); //coordinates of d
datapoints1.push({ x: 1, y: 1 }); //coordinates of a
graphline("l1", datapoints1, "x-axis", "y-axis");
/*

    a___________b
    |           |
    |           |
    |           |
    |___________|
    d           c

    a(1,1)
    b(2,1)
    c(2,2)
    d(1,2)
    a(1,1) .....again point a for completing square.


*/ 
//# sourceMappingURL=linegraph.js.map