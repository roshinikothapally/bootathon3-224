var datapoints1 = [];
var datapoints2 = [];
for (var i = 0; i <= 10; i++) {
    datapoints1.push({ x: i, y: i * i });
    datapoints2.push({ x: i, y: i * i + i });
}
drawgraph("l1", datapoints1, "x-axis", "x-squared");
function draw1() {
    drawgraph2("l2", datapoints1, datapoints2, "X-axis", "Y-axis", "comparison", "x^2", "x^2+x");
}
//# sourceMappingURL=graphs.js.map