var count = document.getElementById("num");
var table = document.getElementById("table");
function dynamic() {
    var num = +(count.value);
    while (table.rows.length > 1) {
        table.deleteRow(1);
    }
    console.log(num);
    for (let i = 1; i <= num; i++) {
        var row = table.insertRow(); // insert a row
        var cell1 = row.insertCell(); // insert cell in row
        var cell2 = row.insertCell(); // insert cell in row
        var cell3 = row.insertCell(); // insert cell in row
        cell1.innerHTML = num.toString() + " * " + i.toString();
        cell2.innerHTML = " = ";
        cell3.innerHTML = (num * i).toString();
    }
}
//# sourceMappingURL=dynamic.js.map